# README #

I used [this project](https://www.youtube.com/watch?v=evIMjbI0ayo) as starting point of developing this cutting tool. Great thanks for original author.

You can open and edit this sources using [SolveSpace](http://solvespace.com/). This is really powerful free (in terms of free software) parametric solid CAD with many useful features like sketch-based solid shape modeling with constraints and assembly with interactive kinematic models.

At first you can look [this video](https://vimeo.com/161012508) on vimeo, which demonstrates assembled tool in action.

This source distributed in a public domain, anybody can use, modify and redistribute it according to his purpose.